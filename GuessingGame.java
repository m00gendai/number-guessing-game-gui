import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GuessingGame extends JFrame {
	private JTextField txtGuess;
	private JLabel lblOutput;
	private JLabel lblTries;
	private int theNumber;
	private int count;

	public void checkGuess() {
		String guessText = txtGuess.getText();
		String message = "";
		String tryMessage = " tries so far";

		try {
			int guess = Integer.parseInt(guessText);

			if (guess < 1) {
				message = "No numbers smaller than 1 allowed";
				tryMessage = count + " tries so far";
			} else if ( guess > 100) {
				message = "No numbers larger than 100 allowed";
				tryMessage = count + " tries so far";
			} else if (guess < theNumber) {
				message = guess + " is too low. Try again.";
				count++;
				tryMessage = count + " tries so far";
			} else if (guess > theNumber) {
				message = guess + " is too high. Try again.";
				count++;
				tryMessage = count + " tries so far";
			} else  {
				count++;
				tryMessage = "You needed " + count + " tries!";
				lblTries.setFont(new Font("Dialog", Font.PLAIN, 12));
				message = guess + " is correct. You are winner. Play again?";
			}

			lblOutput.setText(message);
			lblTries.setText(tryMessage);
			txtGuess.requestFocus();
			txtGuess.selectAll();

		} catch (Exception e) {
			message = "Please enter a whole number between 1 and 100 only.";
			
		} finally {
			lblOutput.setText(message);
			lblTries.setText(tryMessage);
			txtGuess.requestFocus();
			txtGuess.selectAll();
			
		}
	}

	public void newGame() {
		theNumber = (int)(Math.random()*100+1);
		count = 0;
		txtGuess.setText("");
		txtGuess.requestFocus();
		lblOutput.setText("Guess a number between 1 and 100:");
		lblTries.setFont(new Font("Dialog", Font.BOLD, 20));
		lblTries.setText("0 tries so far");
	}

	public GuessingGame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Marcels Number Guessing Game");
		getContentPane().setLayout(null);

		JLabel lblGuessANumber = new JLabel("Guess a number between 1 and 100:");
		lblGuessANumber.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblGuessANumber.setHorizontalAlignment(SwingConstants.CENTER);
		lblGuessANumber.setBounds(12, 14, 422, 15);
		getContentPane().add(lblGuessANumber);

		txtGuess = new JTextField();
		txtGuess.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkGuess();
			}
		});
		txtGuess.setBounds(185, 43, 75, 40);
		getContentPane().add(txtGuess);
		txtGuess.setColumns(10);

		JButton btnGuess = new JButton("Guess");
		btnGuess.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkGuess();
			}
		});
		btnGuess.setBounds(164, 97, 117, 25);
		getContentPane().add(btnGuess);

		JButton btnPlayAgain = new JButton("Play again");
		btnPlayAgain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent f) {
				newGame();
			}
		});
		btnPlayAgain.setBounds(164, 225, 117, 25);
		getContentPane().add(btnPlayAgain);


		lblOutput = new JLabel("Enter a number and click \"Guess\"");
		lblOutput.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblOutput.setHorizontalAlignment(SwingConstants.CENTER);
		lblOutput.setBounds(12, 136, 422, 15);
		getContentPane().add(lblOutput);

		lblTries = new JLabel("0 tries so far");
		lblTries.setHorizontalAlignment(SwingConstants.CENTER);
		lblTries.setFont(new Font("Dialog", Font.BOLD, 20));
		lblTries.setBounds(122, 165, 202, 46);
		getContentPane().add(lblTries);
	}

	public static void main(String[] args) {
		GuessingGame theGame = new GuessingGame();
		theGame.newGame();
		theGame.setSize(new Dimension(450,300));
		theGame.setVisible(true);
	}
}


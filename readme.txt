Number Guessing Game

Exercise from the book "Learn Java the Easy Way" by Bryson Payne

Features:
- Java Swing GUI
- Event Listeners for Keyboard and Mouse input
- Error handling
- Dynamic labels
- Counter (additional challenge, without book reference)
- Handling for out of bounds numbers (additional challenge, without book reference)
- Handling for game reset (additional challenge, without book reference)

Written in Eclipse IDE